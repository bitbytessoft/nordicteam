// JavaScript Document
/* *************************************** */  
/* Way Points JS */
/* *************************************** */  

$(document).ready(function(){
	
	// Hero's Way Points

	$('.heading').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
		
	// Feature Way Points
	$('.nordic_main').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '65%' 
	});
	
	// Author Content Way Points 
	$('.service-head').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '75%' 
	});
	
	// Model Way Points
	$('.Description').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceInLeft');
	}, { 
		offset: '80%' 
	});
	
	$('.model-container p').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceInRight');
	}, { 
		offset: '80%'
	});
	
	$('.model-container a').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceInRight');
	}, { 
		offset: '75%' 
	});
	
	// Service Way Points
	$('.service-item').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '70%' 
	});
	
	// Company Way Points
	$('.company-content').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '50%' 
	});

	// Blog Way Points
	$('.blog-post').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '60%' 
	});
	
	// About Us Way Points
	$('.about-member').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '70%' 
	});
	
	// Model Two Way Points
	$('.model-two p').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeIn');
	}, { 
		offset: '60%' 
	});
	
	// Model Three Way Points With Count To()
	$('.m-counter').waypoint(function(down){
		if(!$(this).hasClass('stop-counter'))
		{
			$(this).countTo();
			$(this).addClass('stop-counter');
		}
	}, { 
		offset: '80%' 
	});
	
	// Pricing Way Points
	$('.pricing-item').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	// Branch Information and Map Location Way Points 
	$('.branch-info').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('flipInY');
	}, { 
		offset: '75%' 
	});
	
	$('.branch-map-area i').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	$('.branch-map-area span').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	// Video Caption and Video Way Points 
	$('.video-cap-container').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('slideInLeft');
	}, { 
		offset: '70%' 
	});
	
	$('.video-container').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('slideInRight');
	}, { 
		offset: '70%' 
	});
	
	// Model Five Item Way Points 
	$('.m-five-item').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	// Grid Item Way Points
	$('.grid .grid-entry').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '90%' 
	});
	
});